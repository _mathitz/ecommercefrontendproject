//START GULP
//Diretorio DEV  : ./src
//Diretorio PROD : /assets
// _ = partial

"use strict";

//Diretorios
let path = {
  dev: "./_src",
  prod: "./assets",
  proxy: "http://projetos.osf"
};

let buildpath = "./assets";

//Carregamento de modulos
let gulp = require("gulp");
let browserSync = require("browser-sync").create();
let sass = require("gulp-sass");
let uglify = require("gulp-uglify");
let imagemin = require("gulp-imagemin");
let changed = require("gulp-changed");
let sourcemaps = require("gulp-sourcemaps");
let concat = require("gulp-concat");
let cssnano = require("gulp-cssnano");
let browsersup = [
  "Android >= 2.3",
  "BlackBerry >= 7",
  "Chrome >= 9",
  "Firefox >= 4",
  "Explorer >= 9",
  "iOS >= 5",
  "Opera >= 11",
  "Safari >= 5",
  "ChromeAndroid >= 9",
  "FirefoxAndroid >= 4",
  "ExplorerMobile >= 9"
];

// ================================================
// TASKS
// ================================================

//BrowserSync
gulp.task("sinc", function() {
  browserSync.init({
    server: {
      baseDir: "./"
    },
    // proxy: "local.dev",
    options: {
      reloadDelay: 250
    },
    notify: true
  });
});

//Compila arquivos scss/sass para css
gulp.task("sass", function() {
  return setTimeout(function() {
    return (
      gulp
        .src(path.dev + "/scss/index.scss")
        //local do arquivo SCSS do ambiente de desenvolvimento e suas subpastas
        .pipe(
          sass({
            sourcemap: true,
            outputStyle: "expanded",
            includePaths: [path.dev + "/scss/**/*"]
          })
        )
        .pipe(
          cssnano({
            autoprefixer: { browsers: browsersup, add: true }
          })
        )
        .pipe(sourcemaps.write())
        //nome do arquivo css finalizado
        .pipe(concat("app.min.css"))
        //local para salvar o CSS final
        .pipe(gulp.dest(path.prod + "/css"))
        //notifica o browserSync para atualizar
        .pipe(browserSync.reload({ stream: true }))
    );
  }, 500);
  //Arquivo SCSS principal que deve importar todos os outros
});

//compiling our Javascripts
gulp.task("scripts", function() {
  //this is where our dev JS scripts are
  return (
    gulp
      .src([
        "!" + path.dev + "/js/_excludes/**/*.js",
        path.dev + "/js/_includes/**/*.js",
        path.dev + "/js/*.js"
      ])
      //this is the filename of the compressed version of our JS
      .pipe(concat("main.min.js"))
      //compress :D
      .pipe(uglify())
      //where we will store our finalized, compressed script
      .pipe(gulp.dest(path.prod + "/js"))
      //notify browserSync to refresh
      .pipe(browserSync.reload({ stream: true }))
  );
});

//Imagens minificadas
gulp.task("images", function() {
  return gulp
    .src(path.dev + "/imgs/*")
    .pipe(changed(path.prod + "/imgs"))
    .pipe(
      imagemin({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true,
        svgoPlugins: [{ removeViewBox: false }]
      })
    )
    .pipe(gulp.dest(path.prod + "/imgs"));
});

//Task default
gulp.task("default", ["sinc", "scripts", "images", "sass"], function() {
  gulp.watch(path.dev + "/scss/**/*.scss", ["sass"]);
  gulp.watch(path.dev + "/js/*.js", ["scripts"]);
  gulp.watch(path.dev + "/imgs/*", ["images"]);
  gulp.watch("*.html").on("change", browserSync.reload);
});
